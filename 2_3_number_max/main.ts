export function Max3Number(a: number, b: number, c: number) {
  if (a > b && a > c) {
    return "a";
  }
  if (b > a && b > c) {
    return "b";
  }

  return "c";
}
