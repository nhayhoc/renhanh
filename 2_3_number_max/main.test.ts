import { Max3Number } from "./main";

describe("Max3Number", () => {
  it("1,2,3 => c", () => {
    expect(Max3Number(1, 2, 3)).toBe("c");
  });
  it("4,2,3 => a", () => {
    expect(Max3Number(4, 2, 3)).toBe("a");
  });
  it("1,20,3 => b", () => {
    expect(Max3Number(1, 20, 3)).toBe("b");
  });
});
