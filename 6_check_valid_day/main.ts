import { isNamNhuan } from "../5_nam_nhuan/main";

export function CheckValidDay(d: number, m: number, y: number): boolean {
  if (m > 12) {
    return false;
  }

  if ((m == 4 || m == 6 || m == 9 || m == 11) && d > 30) {
    return false;
  }
  if (m != 2 && d > 31) {
    return false;
  }
  if ((m == 2 && isNamNhuan(y) && d > 29) || (!isNamNhuan(y) && d > 28)) {
    return false;
  }
  return true;
}

CheckValidDay(30, 1, 2000);
