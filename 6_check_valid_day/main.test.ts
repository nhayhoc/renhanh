import { CheckValidDay } from "./main";

describe("CheckValidDay", () => {
  it("1/13/2000", () => {
    expect(CheckValidDay(1, 13, 2000)).toBe(false);
  });
  it("30/1/2000", () => {
    expect(CheckValidDay(30, 1, 2000)).toBe(true);
  });
  it("31/1/2000", () => {
    expect(CheckValidDay(31, 1, 2000)).toBe(true);
  });
  it("31/4/2000", () => {
    expect(CheckValidDay(31, 4, 2000)).toBe(false);
  });
  it("30/4/2000", () => {
    expect(CheckValidDay(30, 4, 2000)).toBe(true);
  });
  it("31/2/2000", () => {
    expect(CheckValidDay(31, 2, 2000)).toBe(false);
  });
  it("29/2/2000", () => {
    expect(CheckValidDay(29, 2, 2000)).toBe(true);
  });
  it("20/2/2000", () => {
    expect(CheckValidDay(20, 2, 2000)).toBe(true);
  });
  it("29/2/2001", () => {
    expect(CheckValidDay(29, 2, 2001)).toBe(false);
  });
  it("28/2/2001", () => {
    expect(CheckValidDay(28, 2, 2001)).toBe(true);
  });
});
