import { isEven } from "./main";

describe("isEven", () => {
  it("0", () => {
    expect(isEven(0)).toBe(true);
  });
  it("1", () => {
    expect(isEven(1)).toBe(false);
  });
  it("2", () => {
    expect(isEven(2)).toBe(true);
  });
  it("200", () => {
    expect(isEven(200)).toBe(true);
  });
  it("423", () => {
    expect(isEven(423)).toBe(false);
  });
});
