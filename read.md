1. Nhập 2 số a b khác nhau, trả về `"a"` nếu a lớn nhất hoặc `"b"` nếu b lớn nhất
2. Nhập 3 số a b c khác nhau, trả về `"a"` nếu a lớn nhất hoặc `"b"` nếu b lớn nhất hoặc `"c"` nếu c lớn nhất
3. Nhập 1 số,trả về `true` nếu là số chẵn
4. Nhập 2 số a b, trả về `true` nếu có 1 số âm và 1 số dương
5. Nhập 1 số, trả về `true` nếu đó 1 năm nhuận
6. Nhập 3 số d, m, y, trả về `true` nếu 3 số đó có tạo thành 1 ngày/tháng/năm hợp lệ (vd 31/2/2022 là sai)
7. Nhập 3 số, trả về `true` nếu đó là chiều dài của 3 cạnh trong 1 tam giác
8. Nhập 3 số, trả về `true` nếu đó là của tam giác đều
9. Nhập 3 số, trả về `true` nếu đó là của tam giác vuông
10. Nhập 2 số tương ứng là a, và b. Giải phương tình ax + b = 0: trả về

- `vo_so` nếu pt vô số nghiệm,
- `vo_nghiem` nếu pt vô nghiệm,
- {x} nếu pt có 1 nghiệm

11. Nhập 3 số tương ứng là a,b,c. Giải phương trình a.x^2 + b.x + c = 0.

- `vo_so` nếu pt vô số nghiệm,
- `vo_nghiem` nếu pt vô nghiệm,
- {x1,x2} nếu pt có 2 nghiệm phân biệt,
- {x} nếu pt có 1 nghiệm

12. Nhập x, trả về số tiền cần trả với x số điện
    Bậc thang 1 1678 50
    Bậc thang 2 1734 50
    Bậc thang 3 2014 100
    Bậc thang 4 2536 100
    Bậc thang 5 2834 100
    Bậc thang 6 2927 0
