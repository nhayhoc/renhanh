import { isNamNhuan } from "./main";

describe("isNamNhuan", () => {
  it("2016 nhuan", () => {
    expect(isNamNhuan(2016)).toBe(true);
  });
  it("2000 nhuan", () => {
    expect(isNamNhuan(2000)).toBe(true);
  });
  it("500 k nhuan", () => {
    expect(isNamNhuan(500)).toBe(false);
  });
  it("2004 nhuan", () => {
    expect(isNamNhuan(2004)).toBe(true);
  });
});
