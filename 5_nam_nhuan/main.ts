export function isNamNhuan(x: number): boolean {
  if (x % 400 == 0 || (x % 4 == 0 && x % 100 != 0)) {
    return true;
  } else {
    return false;
  }
}
