import { numberGt } from "./main";

describe("numberGt", () => {
  it('1, 2 return "b"', () => {
    expect(numberGt(1, 2)).toBe("b");
  });
  it('2, 1 return "a"', () => {
    expect(numberGt(2, 1)).toBe("a");
  });
  it('200, 2000 return "a"', () => {
    expect(numberGt(200, 2000)).toBe("b");
  });
});
